import {Request} from './requestHeader';
/**
 * @author wahyu fatur rizki | https://gitlab.com/wahyufaturrizky
 * @return { obj }
 * Retrieve Data
 * return async
 */

export const getAllUsers = () => {
  const result = Request().get('/items/users');
  return result;
};

export const getAllRoles = () => {
  const result = Request().get('/items/role');
  return result;
};

export const addNewUser = (data) => {
  const result = Request().post('/items/users', data);
  return result;
};

export const addNewRole = (data) => {
  const result = Request().post('/items/role', data);
  return result;
};
