import axios from 'axios';
import Config from 'react-native-config';

/**
 * @author wahyu fatur rizki | https://gitlab.com/wahyufaturrizky
 * @return { obj }
 * Custom Header axios,
 * create from
 * using this function should axios().get(values)
 */

export const Request = () => {
  return axios.create({
    baseURL: Config.API_URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
};
