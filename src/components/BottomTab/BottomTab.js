import {Text} from 'components/Text';
import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import {ColorBaseEnum, ColorBaseGrayEnum} from 'styles/Colors';
import {BorderRadiusEnum, PaddingEnum, SizeEnum} from 'styles/Spacer';

const BottomTab = (props) => {
  const title = props.route.params?.title || props.route.name;

  const goToUser = () => {
    props.navigation.navigate('Users', {title: 'Users'});
  };
  const goToHomePage = () => {
    props.navigation.navigate('HomePage', {title: 'HomePage'});
  };
  return (
    <View
      style={[
        styles.shadow,
        {
          backgroundColor: ColorBaseEnum.white,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          paddingTop: PaddingEnum['1x'],
          paddingBottom: PaddingEnum['1x'],
          borderTopRightRadius: BorderRadiusEnum['3x'],
          borderTopLeftRadius: BorderRadiusEnum['3x'],
        },
      ]}>
      <TouchableOpacity onPress={goToHomePage}>
        <View style={{alignItems: 'center'}}>
          <IconAntDesign
            name="home"
            size={SizeEnum['5x']}
            color={
              title === 'Home Page' || title === 'HomePage'
                ? '#034371'
                : ColorBaseGrayEnum.gray500
            }
          />
          <Text
            label="Role"
            color={
              title === 'Home Page' || title === 'HomePage'
                ? '#034371'
                : ColorBaseGrayEnum.gray500
            }
            variant="micro"
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={goToUser}>
        <View style={{alignItems: 'center'}}>
          <IconAntDesign
            name="user"
            size={SizeEnum['5x']}
            color={
              title === 'Users' || title === 'Users'
                ? '#034371'
                : ColorBaseGrayEnum.gray500
            }
          />
          <Text
            label="Users"
            color={
              title === 'Users' || title === 'Users'
                ? '#034371'
                : ColorBaseGrayEnum.gray500
            }
            variant="micro"
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default BottomTab;

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },
});
