/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Alert} from 'react-native';
import {Header, Input} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import {iOSUIKit, materialColors} from 'react-native-typography';
import Loading from '../../components/loading';
import {addNewRole} from '../../services/retrieveData';
import SweetAlert from 'react-native-sweet-alert';
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';

function AddNewRole(props) {
  let page = props.route.params.title;
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    title: '',
    description: '',
    requirement: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);

  const {title, description, requirement} = state;

  useEffect(() => {
    if (isFocused) {
      checkConnectionIsOffline();
    }
  }, [isFocused]);

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const handleAddNewNote = async () => {
    const valueNote = [
      {
        name: 'title',
        value: title,
      },
      {
        name: 'description',
        value: description,
      },
      {
        name: 'requirement',
        value: requirement,
      },
    ];

    let emptyField = valueNote.find((value) => value.value === '');

    if (!emptyField) {
      setIsLoading(true);
      const data = {
        title: title,
        description: description,
        requirement: requirement,
      };
      const response = await addNewRole(data).catch((error) => {
        const {status} = error.response;
        if (status === 500) {
          setIsLoading(false);
          SweetAlert.showAlertWithOptions(
            {
              title: 'Failed Create',
              subTitle: `failed create new note with status ${status}\n\nplease try again`,
              confirmButtonTitle: 'OK',
              confirmButtonColor: '#000',
              otherButtonTitle: 'Cancel',
              otherButtonColor: '#dedede',
              style: 'success',
              cancellable: true,
            },
            (callback) => console.log('callback', callback),
          );
        }
      });

      const {status} = response;

      if (status === 200) {
        setIsLoading(false);

        SweetAlert.showAlertWithOptions(
          {
            title: 'Success Create',
            subTitle: 'Yeay! Your success create new note',
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'success',
            cancellable: true,
          },
          (callback) => props.navigation.navigate('HomePage'),
        );
      }
    } else {
      SweetAlert.showAlertWithOptions(
        {
          title: 'Attention!',
          subTitle: `This field ${emptyField.name} can't be empty`,
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'warning',
          cancellable: true,
        },
        (callback) => console.log('callback', callback),
      );
    }
  };

  return (
    <>
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Header
          containerStyle={{
            backgroundColor: '#fff',
          }}
          leftComponent={{
            icon: 'arrow-back-ios',
            color: materialColors.blackPrimary,
            onPress: () => props.navigation.goBack(),
          }}
          centerComponent={{
            text: `${page}`,
            style: {color: materialColors.blackPrimary},
          }}
        />

        <ScrollView>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              flex: 1,
              paddingTop: 32,
            }}>
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Title
            </Text>
            <Input
              leftIcon={{type: 'materialIcons', name: 'book', color: '#034371'}}
              onChangeText={(val) => handleChange('title', val)}
              placeholder="please fill note title"
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Description
            </Text>
            <Input
              leftIcon={{
                type: 'materialIcons',
                name: 'subtitles',
                color: '#034371',
              }}
              onChangeText={(val) => handleChange('description', val)}
              placeholder="please fill note description"
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Requirement
            </Text>
            <Input
              leftIcon={{
                type: 'materialIcons',
                name: 'subtitles',
                color: '#034371',
              }}
              onChangeText={(val) => handleChange('requirement', val)}
              placeholder="please fill note requirement"
            />
          </View>
        </ScrollView>
        <TouchableOpacity
          onPress={() => handleAddNewNote()}
          style={{
            width: '90%',
            padding: 15,
            borderRadius: 12,
            alignSelf: 'center',
            marginBottom: 16,
            backgroundColor: '#034371',
          }}>
          <Text
            style={[
              {
                textAlign: 'center',
              },
              iOSUIKit.title3White,
            ]}>
            Simpan
          </Text>
        </TouchableOpacity>
      </View>
      {isLoading && <Loading />}
    </>
  );
}

export default AddNewRole;

const styles = StyleSheet.create({
  modalText: {
    marginBottom: 20,
    textAlign: 'left',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
