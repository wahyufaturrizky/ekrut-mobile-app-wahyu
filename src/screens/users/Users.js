/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import {Avatar, Icon} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import {iOSUIKit, materialColors} from 'react-native-typography';
import {getAllUsers} from 'services/retrieveData';
import Loading from 'components/loading';
import {useIsFocused} from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';
import SweetAlert from 'react-native-sweet-alert';
import BottomTab from 'components/BottomTab/BottomTab';
import {PaddingEnum} from 'styles/Spacer';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Users(props) {
  const isFocused = useIsFocused();
  const {height} = Dimensions.get('window');
  const [stateAllRole, setStateAllRole] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    if (isFocused) {
      fetchDataAllNotes();
      checkConnectionIsOffline();
    }
  }, [isFocused]);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    fetchDataAllNotes();

    wait(2000).then(() => setRefreshing(false));
  }, []);

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const fetchDataAllNotes = async () => {
    setIsLoading(true);
    response = await getAllUsers().catch((error) => {
      setIsLoading(false);
    });

    const {status, data} = response;

    if (status === 200) {
      setStateAllRole(data.data);
      setIsLoading(false);
    }
  };

  return (
    <>
      <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
        <ImageBackground
          style={{
            justifyContent: 'flex-start',
            paddingVertical: PaddingEnum['2x'],
            backgroundColor: '#034371',
          }}>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
            }}>
            <Avatar
              size="large"
              rounded
              icon={{name: 'user', type: 'ant-design'}}
              onPress={() => alert('this is your Avatar Photo')}
              activeOpacity={0.7}
              overlayContainerStyle={{backgroundColor: '#011E32'}}
            />

            <Text style={[iOSUIKit.subheadWhite, {marginVertical: 10}]}>
              Hello Selamat Datang Wahyu Fatur Rizki!
            </Text>
            <Text style={[iOSUIKit.title3EmphasizedWhite]}>Daftar User</Text>
          </View>
        </ImageBackground>

        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: height * -0.02,
              flex: 1,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: 30,
              }}>
              <Text
                style={[
                  iOSUIKit.subheadEmphasized,
                  {color: materialColors.blackSecondary},
                ]}>
                Semua users anda
              </Text>
            </View>

            <View style={{position: 'relative', marginBottom: 80}}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderRadius: 12,
                  paddingHorizontal: 14,
                  paddingVertical: 24,
                }}>
                <Text
                  style={[
                    iOSUIKit.subheadEmphasized,
                    {color: materialColors.blackPrimary},
                    {marginBottom: 14, marginLeft: 14},
                  ]}>
                  Total Role berjumlah: {stateAllRole.length}
                </Text>
                {stateAllRole.length !== 0 ? (
                  stateAllRole.map((value, key) => (
                    <TouchableOpacity
                      key={key}
                      onPress={() =>
                        props.navigation.navigate('DetailUser', {
                          dataRow: value,
                          titlePge: 'Detail User',
                        })
                      }>
                      <View>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            borderBottomColor: '#E5E5E5',
                            borderBottomWidth: 2,
                            paddingBottom: 8,
                            marginTop: 8,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <Icon
                              reverse
                              name="description"
                              type="materialIcons"
                              color="#034371"
                            />
                            <View style={{marginLeft: 12}}>
                              <Text
                                textTransform="capitalize"
                                style={[
                                  iOSUIKit.bodyEmphasized,
                                  {color: materialColors.blackPrimary},
                                ]}>
                                {value.fullname || 'empty fullname'}
                              </Text>
                              <Text
                                textTransform="capitalize"
                                style={[
                                  iOSUIKit.footnote,
                                  {color: materialColors.blackTertiary},
                                ]}>
                                {value.email || 'empty email'}
                              </Text>
                            </View>
                          </View>
                          <View>
                            <Text
                              textTransform="capitalize"
                              style={[iOSUIKit.footnote, {color: '#034371'}]}>
                              See detail
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  ))
                ) : (
                  <View>
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackTertiary},
                        {
                          justifyContent: 'center',
                          textAlign: 'center',
                        },
                      ]}>
                      {isOffline
                        ? 'Ooppss your offline... '
                        : 'Please wait get all data notes ...'}
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={{position: 'absolute', bottom: 10, right: 10}}>
          <Icon
            reverse
            name="plus"
            type="font-awesome"
            color="#F54291"
            onPress={() =>
              props.navigation.navigate('AddNewUser', {
                title: 'Add New User',
              })
            }
          />
        </View>
      </View>
      <BottomTab {...props} />
      {isLoading && <Loading />}
    </>
  );
}

export default Users;
