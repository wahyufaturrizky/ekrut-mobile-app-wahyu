/* eslint-disable */
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Header, Icon, Input} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import SweetAlert from 'react-native-sweet-alert';
import {iOSUIKit, materialColors} from 'react-native-typography';
import Loading from '../../components/loading';

function DetailUser(props) {
  let dataRow = props.route.params.dataRow;
  let titlePge = props.route.params.titlePge;
  const isFocused = useIsFocused();
  const {height} = Dimensions.get('window');
  const [stateNote, setStateNote] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);
  const [isEditNote, setIsEditNote] = useState(false);
  const [isStatusButtonUpdate, setIsStatusButtonUpdate] = useState('');

  useEffect(() => {
    if (isFocused || isStatusButtonUpdate === '') {
      checkConnectionIsOffline();
    }
    if (isEditNote) {
      setIsStatusButtonUpdate('Save Update');
    }
    if (!isEditNote) {
      setIsStatusButtonUpdate('Update Note');
    }
  }, [isFocused, isEditNote, isStatusButtonUpdate]);

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const [state, setState] = useState({
    title: '',
    detail: '',
    _id: '',
    createdAt: '',
    updatedAt: '',
    __v: '',
  });

  const {title, detail, _id} = state;

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const handleUpdateNote = async () => {
    setIsLoading(true);

    const data = {
      title: title,
      detail: detail,
      noteId: _id,
    };

    response = await addNewNote(data).catch((error) => {
      setIsLoading(false);
    });

    const {status} = response;

    if (status === 200) {
      setIsLoading(false);

      SweetAlert.showAlertWithOptions(
        {
          title: 'Success Update',
          subTitle: 'Yeay! Your success update note',
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'success',
          cancellable: true,
        },
        (callback) => props.navigation.navigate('HomePage'),
      );
    }
  };

  return (
    <>
      <View style={{flex: 1, backgroundColor: '#E5E5E5'}}>
        <Header
          containerStyle={{
            backgroundColor: '#fff',
          }}
          leftComponent={{
            icon: 'arrow-back-ios',
            color: materialColors.blackPrimary,
            onPress: () => props.navigation.goBack(),
          }}
          centerComponent={{
            text: `${titlePge}`,
            style: {color: materialColors.blackPrimary},
          }}
        />

        <ScrollView>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: height * 0.04,
              flex: 1,
            }}>
            <View style={{position: 'relative'}}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderRadius: 12,
                  paddingHorizontal: 14,
                  paddingVertical: 24,
                }}>
                <Text
                  style={[
                    iOSUIKit.subheadEmphasized,
                    {color: materialColors.blackPrimary},
                    {marginBottom: 14, marginLeft: 14},
                  ]}>
                  {isEditNote ? 'Update Note' : 'Detail Note'}
                </Text>
                {isEditNote ? (
                  <View>
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary},
                      ]}>
                      Title
                    </Text>
                    <Input
                      leftIcon={{
                        type: 'materialIcons',
                        name: 'book',
                        color: '#034371',
                      }}
                      onChangeText={(val) => handleChange('title', val)}
                      placeholder="please fill note title"
                      defaultValue={title}
                    />
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary},
                      ]}>
                      Detail
                    </Text>
                    <Input
                      leftIcon={{
                        type: 'materialIcons',
                        name: 'subtitles',
                        color: '#034371',
                      }}
                      onChangeText={(val) => handleChange('detail', val)}
                      placeholder="please fill note detail"
                      defaultValue={detail}
                    />
                  </View>
                ) : (
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color="#034371"
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        {dataRow.fullname || 'empty fullname'}
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        Phone: {dataRow.phone || 'empty phone'}
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        Email: {dataRow.email || 'empty email'}
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        Address: {dataRow.address || 'empty address'}
                      </Text>
                    </View>
                  </View>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
        {/* <TouchableOpacity
          onPress={() => {
            setIsEditNote(!isEditNote);
            if (isStatusButtonUpdate === 'Save Update') {
              handleUpdateNote();
            }
          }}
          style={{
            width: '90%',
            padding: 15,
            borderRadius: 12,
            alignSelf: 'center',
            backgroundColor: '#034371',
            marginBottom: 14,
          }}>
          <Text
            style={[
              {
                textAlign: 'center',
              },
              iOSUIKit.title3White,
            ]}>
            {isStatusButtonUpdate === 'Save Update'
              ? 'Save Update'
              : 'Update note'}
          </Text>
        </TouchableOpacity> */}
      </View>
      {isLoading && <Loading />}
    </>
  );
}

export default DetailUser;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e5e5e5',
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
