/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Alert} from 'react-native';
import {Header, Input} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import {iOSUIKit, materialColors} from 'react-native-typography';
import Loading from '../../components/loading';
import {addNewUser} from '../../services/retrieveData';
import SweetAlert from 'react-native-sweet-alert';
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';

function AddNewUser(props) {
  let page = props.route.params.title;
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    fullname: '',
    phone: '',
    email: '',
    address: '',
    type: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);

  const {fullname, phone, email, address, type} = state;

  useEffect(() => {
    if (isFocused) {
      checkConnectionIsOffline();
    }
  }, [isFocused]);

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const handleAddNewNote = async () => {
    const valueNote = [
      {
        name: 'fullname',
        value: fullname,
      },
      {
        name: 'phone',
        value: phone,
      },
      {
        name: 'email',
        value: email,
      },
      {
        name: 'address',
        value: address,
      },
      {
        name: 'type',
        value: type,
      },
    ];

    let emptyField = valueNote.find((value) => value.value === '');

    if (!emptyField) {
      setIsLoading(true);
      const data = {
        fullname: fullname,
        phone: phone,
        email: email,
        address: address,
        type: type,
      };
      const response = await addNewUser(data).catch((error) => {
        const {status} = error.response;
        if (status === 500) {
          SweetAlert.showAlertWithOptions(
            {
              title: 'Failed Create',
              subTitle: `failed create new note with status ${status}\n\nplease try again`,
              confirmButtonTitle: 'OK',
              confirmButtonColor: '#000',
              otherButtonTitle: 'Cancel',
              otherButtonColor: '#dedede',
              style: 'warning',
              cancellable: true,
            },
            (callback) => console.log('callback', callback),
          );
        }
        if (status === 400) {
          SweetAlert.showAlertWithOptions(
            {
              title: 'Failed Create',
              subTitle:
                error.response.data.errors &&
                error.response.data.errors[0].message,
              confirmButtonTitle: 'OK',
              confirmButtonColor: '#000',
              otherButtonTitle: 'Cancel',
              otherButtonColor: '#dedede',
              style: 'warning',
              cancellable: true,
            },
            (callback) => console.log('callback', callback),
          );
        }
        setIsLoading(false);
      });

      console.log('@response', response);

      const {status} = response;

      if (status && status === 200) {
        setIsLoading(false);

        SweetAlert.showAlertWithOptions(
          {
            title: 'Success Create',
            subTitle: 'Yeay! Your success create new note',
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'success',
            cancellable: true,
          },
          (callback) => props.navigation.navigate('Users'),
        );
      }
    } else {
      SweetAlert.showAlertWithOptions(
        {
          title: 'Attention!',
          subTitle: `This field ${emptyField.name} can't be empty`,
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'warning',
          cancellable: true,
        },
        (callback) => console.log('callback', callback),
      );
    }
  };

  return (
    <>
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Header
          containerStyle={{
            backgroundColor: '#fff',
          }}
          leftComponent={{
            icon: 'arrow-back-ios',
            color: materialColors.blackPrimary,
            onPress: () => props.navigation.goBack(),
          }}
          centerComponent={{
            text: `${page}`,
            style: {color: materialColors.blackPrimary},
          }}
        />

        <ScrollView>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              flex: 1,
              paddingTop: 32,
            }}>
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Full Name
            </Text>
            <Input
              leftIcon={{type: 'materialIcons', name: 'book', color: '#034371'}}
              onChangeText={(val) => handleChange('fullname', val)}
              placeholder="please fill note full name"
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Phone
            </Text>
            <Input
              leftIcon={{
                type: 'materialIcons',
                name: 'subtitles',
                color: '#034371',
              }}
              onChangeText={(val) => handleChange('phone', val)}
              placeholder="please fill note phone"
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Email
            </Text>
            <Input
              leftIcon={{
                type: 'materialIcons',
                name: 'subtitles',
                color: '#034371',
              }}
              onChangeText={(val) => handleChange('email', val)}
              placeholder="please fill note email"
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Address
            </Text>
            <Input
              leftIcon={{
                type: 'materialIcons',
                name: 'subtitles',
                color: '#034371',
              }}
              onChangeText={(val) => handleChange('address', val)}
              placeholder="please fill note address"
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Type
            </Text>
            <Input
              leftIcon={{
                type: 'materialIcons',
                name: 'subtitles',
                color: '#034371',
              }}
              onChangeText={(val) => handleChange('type', val)}
              placeholder="please fill note type"
            />
          </View>
        </ScrollView>
        <TouchableOpacity
          onPress={() => handleAddNewNote()}
          style={{
            width: '90%',
            padding: 15,
            borderRadius: 12,
            alignSelf: 'center',
            marginBottom: 16,
            backgroundColor: '#034371',
          }}>
          <Text
            style={[
              {
                textAlign: 'center',
              },
              iOSUIKit.title3White,
            ]}>
            Simpan
          </Text>
        </TouchableOpacity>
      </View>
      {isLoading && <Loading />}
    </>
  );
}

export default AddNewUser;

const styles = StyleSheet.create({
  modalText: {
    marginBottom: 20,
    textAlign: 'left',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
