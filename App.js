import React from 'react';

// Use Navigation Ver. 5.7.0
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

// Screen
import HomePage from './src/screens/homePage/HomePage';
import Users from './src/screens/users/Users';
import AddNewRole from './src/screens/addNewRole/AddNewRole';
import AddNewUser from './src/screens/addNewUser/AddNewUser';
import DetailRole from './src/screens/detailRole/DetailRole';
import DetailUser from './src/screens/detailUser/DetailUser';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomePage" headerMode={'none'}>
        <Stack.Screen name="HomePage" component={HomePage} />
        <Stack.Screen name="Users" component={Users} />
        <Stack.Screen name="AddNewRole" component={AddNewRole} />
        <Stack.Screen name="AddNewUser" component={AddNewUser} />
        <Stack.Screen name="DetailRole" component={DetailRole} />
        <Stack.Screen name="DetailUser" component={DetailUser} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNav;
